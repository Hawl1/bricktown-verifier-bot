"""
sqlite is for remembering and using data from user aaaa
"""
import sqlite3
import random
import string

from discord import app_commands
from dotenv import dotenv_values

import discord
import httpx


async def get_profile(name):
    """
    Gets profile of the user
    """
    async with httpx.AsyncClient() as http_client:
        response = await http_client.get(
            f"https://bricktown-scrape-api.vercel.app/{name}"
        )
        return response.json()


userdb = sqlite3.connect("user.db")

table_cursor = userdb.cursor()

table_cursor.execute(
    """CREATE TABLE IF NOT EXISTS Users (
    DISCORD_ID INTEGER,
    BRICKTOWN_NAME TEXT,
    VERIFY_CODE TEXT
)"""
)

table_cursor.close()
userdb.commit()

env_vars = dotenv_values(".env")

guild_id = env_vars["GUILD_ID"]
verified_role = env_vars["VERIFIED_ROLE_ID"]

intents = discord.Intents.default()
client = discord.Client(intents=intents)
tree = app_commands.CommandTree(client)


@tree.command(
    name="verify", description="Get verified", guild=discord.Object(id=guild_id)
)
async def verify_command(interaction, name: str):
    """
        Verifies the user if user did nothing wrong
    """
    direct_message = await interaction.user.create_dm()

    characters = string.ascii_letters + string.digits
    random_string = "".join(random.choice(characters) for _ in range(15))
    random_string = "bricktown-verify-" + random_string

    userdb.execute(
        """INSERT INTO Users (DISCORD_ID, BRICKTOWN_NAME, VERIFY_CODE)
    VALUES (?, ?, ?)
    """,
        (int(interaction.user.id), str(name), str(random_string)),
    )

    userdb.commit()

    embed = discord.Embed(
        title="Hey!", description="Check DMs now!", color=discord.Color.light_gray()
    )

    await interaction.response.send_message(embed=embed, ephemeral=True)
    embed = discord.Embed(
        title="Hey there",
        description=f"Your bricktown verify code is `{random_string}` "
        "paste this on your bricktown description then run /confirm .",
        color=discord.Color.blue(),
    )
    await direct_message.send(embed=embed)


@tree.command(
    name="confirm", description="Confirm verify", guild=discord.Object(id=guild_id)
)
async def confirm_command(interaction):
    """
    Checks if user putted verify code
    If user putted the code then give the role and set the nick to user
    If not then nah
    """

    cursor = userdb.cursor()

    cursor.execute("SELECT * FROM Users WHERE DISCORD_ID=?", (interaction.user.id,))
    row = cursor.fetchone()

    cursor.close()

    if row is None:
        embed = discord.Embed(
            title="Bruh",
            description="You need to run /verify first",
            color=discord.Color.red(),
        )
        await interaction.response.send_message(embed=embed, ephemeral=True)
    else:
        bt_name = row[1]
        verify_code = row[2]

        profile = await get_prof(bt_name)
        bt_name_capitalized = profile["username"]
        bt_description = profile["description"]

        if verify_code in bt_description:
            embed = discord.Embed(
                title="Verified successfully!",
                description="You will get @Verified role in a second,"
                "have a good time on the server!",
                color=discord.Color.green(),
            )

            cursor = userdb.cursor()
            cursor.execute(
                """UPDATE Users SET VERIFY_CODE="SUCCESS" WHERE DISCORD_ID=?""",
                (str(row[0]),),
            )
            cursor.close()
            userdb.commit()

            await interaction.response.send_message(embed=embed, ephemeral=True)
            role = discord.utils.get(interaction.guild.roles, id=int(verified_role))
            await interaction.user.add_roles(role, reason="Verified their account.")
            await interaction.user.edit(nick=bt_name_capitalized)

        else:
            embed = discord.Embed(
                title="Can't verify",
                description="Please check if you put the verify code"
                "on your Bricktown account's description.",
                color=discord.Color.red(),
            )
            await interaction.response.send_message(embed=embed, ephemeral=True)


@tree.command(
    name="unverify",
    description="Unverifies your bricktown account",
    guild=discord.Object(id=guild_id),
)
async def un_verify_command(interaction):
    """
    Removes the role from user and deletes the row from the database if they joined before.
    """

    cursor = userdb.cursor()

    cursor.execute(
        "SELECT * FROM Users WHERE DISCORD_ID=? AND NOT VERIFY_CODE='SUCCESS'",
        (interaction.user.id,),
    )
    row = cursor.fetchone()

    if row is None:
        embed = discord.Embed(
            title="Bruh",
            description="You aren't even verified :/",
            color=discord.Color.red(),
        )
        await interaction.response.send_message(embed=embed, ephemeral=True)
    else:
        cursor.execute("DELETE FROM Users WHERE DISCORD_ID=?", (interaction.user.id,))
        role = interaction.guild.get_role(int(verified_role))
        await interaction.user.remove_roles(role, reason="Unverified the user")
        embed = discord.Embed(
            title="You have been unverified",
            description="See ya!",
            color=discord.Color.green(),
        )

        cursor.close()
        await interaction.response.send_message(embed=embed, ephemeral=True)


async def on_member_join(member):
    """
    Adds role to member if they joined before.
    """

    if member.guild.id == guild_id:
        cursor = userdb.cursor()
        cursor.execute("SELECT * FROM Users WHERE DISCORD_ID=?", (member.id))
        row = cursor.fetchone()
        cursor.close()

        if row is not None:
            guild = client.get_guild(guild_id)
            role = guild.get_role(int(verified_role))
            await member.add_roles(role, reason="Verified their account already.")
            await member.edit(nick=row[1])


@client.event
async def on_ready():
    """
    Adds presence as 'Playing Bricktown Beta' and adds slash commands.
    """
    await client.change_presence(activity=discord.Game(name="Bricktown Beta"))
    await tree.sync(guild=discord.Object(id=guild_id))


client.run(env_vars["TOKEN"])
