# Bricktown discord verifier bot

[My Bricktown account](https://bricktown.xyz/users/profile/Hawli)

## Features:
- Can verify people
- Licensed under GPLv3

## Hosting
For hosting, i'd recommend [Replit](https://replit.com/)

## How to setup:
1. Create a new Python project on Replit

2. go to shell and run this

```sh
git clone https://gitlab.com/Hawl1/bricktown-verifier-bot/
```

2. Edit the .env file

### Things inside the .env file

**TOKEN**
TOKEN of the bot you will host, make sure you invited the bot to the server you want to setup.

Since you are using Replit i dont recommend doing that lol, instead use secrets lol

**GUILD_ID**
ID of the server you will setup the bot into.

**VERIFIED_ROLE_ID**
ID of the role when user gets verified.

3. Running the bot

You will need to install dependencies

```sh
pip3 install httpx discord
```

Then click on Run button on replit

4. 24/7 Uptime

I'm not going to explain this, you can go to [here](https://docs.replit.com/tutorials/python/build-basic-discord-bot-python) for hosting tutorial.
By the way use [UptimeRobot](https://uptimerobot.com/) for uptime thingy that is mentioned on there. 
